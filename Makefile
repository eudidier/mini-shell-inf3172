##
## INF3172 – Principes des systèmes d'exploitation
## Euloge Nihezagire NIHE16098601
## TP2 – Hiver 2016 
## Shell pour la gestion des fichiers et répertoires en Unix.
## Fichier : Makefile
##

CC = gcc
OPTIONS = -Wall -g  
EXE = tp2

all: lien

compile: structure.o commande.o main.o 

structure.o: structure.c structure.h
	$(CC) $(OPTIONS) -c structure.c 
	
commande.o: commande.c commande.h structure.c
	$(CC) $(OPTIONS) -c commande.c

main.o: main.c structure.c commande.c
	$(CC) $(OPTIONS) -c main.c 

lien: compile
	$(CC) *.o -o $(EXE) -lm

clean:
	rm $(EXE)
	rm *.o

start: lien
	./$(EXE)
