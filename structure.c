/**
 * INF3172 – Principes des systèmes d'exploitation
 *
 * Euloge Nihezagire NIHE16098601 
 * TP2 – Hiver 2016 
 * Shell pour la gestion des fichiers et répertoires en Unix.
 *
 */


#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "structure.h"
#include <assert.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

int ajout_nbLiens(BLOCREPERTOIRE **tab_disque, char *chemin);
int ajout_taille(BLOCREPERTOIRE **tab_disque, char *chemin, int taille_pere);

BLOCREPERTOIRE * creer_repertoire_fichier(BLOCREPERTOIRE **tab_disque, unsigned int position_dir, char **arg_list, char type, char *chemin, int bloc_utiliser[NB_BLOC_FINAL])
{

  int nvl_taille = 0;
  time_t temps_actuel;
  time(&temps_actuel);

  BLOCREPERTOIRE* bloc_dir = NULL;
  LISTEREPERTOIRE* list_dir = NULL;

  bloc_dir = (BLOCREPERTOIRE *) malloc(sizeof(BLOCREPERTOIRE));
  list_dir = (LISTEREPERTOIRE *) malloc(sizeof(LISTEREPERTOIRE));

  nvl_taille = strlen(chemin) + strlen(arg_list[1]) + 1;

  assert(chemin && " erreur d'allocationde de mémoire ");

  list_dir->liste->nom = (char*) calloc(strlen(arg_list[1]) + 1, sizeof(char));
  list_dir->liste->path_dir = (char*) calloc(nvl_taille + 1, sizeof(char));
  list_dir->liste->pere_dir = (char*) calloc(strlen(chemin), sizeof(char));
  strcat(list_dir->liste->pere_dir, chemin);
  strcat(list_dir->liste->nom, arg_list[1]);
  strcat(list_dir->liste->path_dir, chemin);
  if(strcmp(chemin, "/") != 0)
  {
    strcat(list_dir->liste->path_dir, "/");
  }
  strcat(list_dir->liste->path_dir, arg_list[1]);

  list_dir->liste->position = position_dir;
  list_dir->liste->type = type;


  INODE* inode_fichier = NULL;
  TEMPS* time_fichier = (TEMPS *) malloc(sizeof(TEMPS));
  ;
  inode_fichier = (INODE *) malloc(sizeof(INODE));
  inode_fichier->GID = geteuid();
  inode_fichier->UID = getegid();
  time_fichier = localtime(&temps_actuel);
  inode_fichier->acces = *time_fichier;
  inode_fichier->nbLiens = 1;
  inode_fichier->taille = 0;

  ajout_nbLiens(tab_disque, chemin);
  inode_fichier->creation = *time_fichier;
  if(type == 'd')
  {
    strcat(inode_fichier->mode, "drw-r--r--");

  }

  if(type == 'f')
  {
    int i = NB_BLOC_FINAL;
    int j = 0;
    inode_fichier->taille = (double) strtol(arg_list[2], (char **) NULL, 10);
    strcat(inode_fichier->mode, "-rw-r--r--");

    int taille_restant = 0;

    while(i != 0 && j < 10 && j < (int) ceil((float) inode_fichier->taille / 2))
    {

      if(bloc_utiliser[i] == 0)
      {

        inode_fichier->premiersBlocs[j] = i;
        bloc_utiliser[i] = 1;

        j++;
      }
      i--;
    }
    if(inode_fichier->taille > 20 || (inode_fichier->taille > 20 && inode_fichier->taille <= 1044))
    {
      taille_restant = (int) ceil((float) ((inode_fichier->taille) - 20) / 4);
      INDIRECTIONSIMPLE* inode_simpleIndirection = (INDIRECTIONSIMPLE *) calloc(255, sizeof(int));
      j = 0;
      while(i != 0 && j < 256 && 0 < taille_restant)
      {

        if(bloc_utiliser[i] == 0)
        {

          inode_simpleIndirection->no_bloc[j] = i;

          bloc_utiliser[i] = 1;
          j++;
          taille_restant--;
        }
        i--;
      }
      inode_fichier->isimple = inode_simpleIndirection;
    }
    if(inode_fichier->taille > 1044 || (inode_fichier->taille > 1044 && inode_fichier->taille <= 524308))
    {
      INDIRECTIONDOUBLE inode_doubleIndirection[256];
      j = 0;
      int k = 0;

      while(i != 0 && k < 256 && 0 < taille_restant)
      {
        inode_doubleIndirection->is[k] = (INDIRECTIONSIMPLE *) calloc(255, sizeof(int));
        while(i != 0 && j < 256 && 0 < taille_restant)
        {

          if(bloc_utiliser[i] == 0)
          {

            bloc_utiliser[i] = 1;
            inode_doubleIndirection->is[k]->no_bloc[j] = i;
            j++;
            taille_restant--;
          }
          i--;
        }
        j = 0;
        k++;
      }
      inode_fichier->idouble = inode_doubleIndirection;

    }

    if(inode_fichier->taille > 524308 || (inode_fichier->taille > 524308 && inode_fichier->taille <= 134217748))
    {
      INDIRECTIONTRIPLE inode_tribleIndirection[256];
      j = 0;
      int k = 0;
      int t = 0;

      while(i != 0 && t < 256 && 0 <= taille_restant)
      {
        inode_tribleIndirection->id[t] = (INDIRECTIONDOUBLE *) calloc(255, sizeof(INDIRECTIONSIMPLE));
        while(i != 0 && k < 256 && 0 < taille_restant)
        {
          inode_tribleIndirection->id[t]->is[k] = (INDIRECTIONSIMPLE *) calloc(255, sizeof(int));

          while(i != 0 && j < 256 && 0 < taille_restant)
          {

            if(bloc_utiliser[i] == 0)
            {
              bloc_utiliser[i] = 1;
              inode_tribleIndirection->id[t]->is[k]->no_bloc[j] = i;
              j++;
              taille_restant--;
            }
            i--;
          }
          j = 0;
          k++;
        }
        k = 0;
        t++;
      }


      inode_fichier->itriple = inode_tribleIndirection;
    }

    ajout_taille(tab_disque, chemin, inode_fichier->taille);
  }


  list_dir->liste->inode = inode_fichier;

  list_dir->suivant = NULL;
  tab_disque[position_dir - 1]->repertoire->suivant = list_dir;
  bloc_dir->repertoire = list_dir;

  return bloc_dir;

}

int ajout_nbLiens(BLOCREPERTOIRE **tab_disque, char *chemin)
{
  int position = -1;
  int i;
  for(i = 0;i < strlen(chemin);i++)
  {
    if(chemin[i] == '/')
    {
      position = i;
    }
  }
  char *chem_dema = (char*) calloc(strlen(chemin), sizeof(char));
  strncpy(chem_dema, chemin, position);
  position++;
  int j = 0;
  if(strcmp(chemin, "/") != 0)
  {

    while(1)
    {
      if(strcmp(tab_disque[j]->repertoire->liste->path_dir, chemin) == 0)
      {
        if(strcmp(tab_disque[j]->repertoire->liste->nom, chemin + position) == 0)
        {
          tab_disque[j]->repertoire->liste->inode->nbLiens++;
          return 1;
        }
      }
      j++;
    }
  }
  else
  {
    tab_disque[j]->repertoire->liste->inode->nbLiens++;
  }

  return -1;
}

int ajout_taille(BLOCREPERTOIRE **tab_disque, char *chemin, int taille_pere)
{
  int position = -1;
  int i;
  for(i = 0;i < strlen(chemin);i++)
  {
    if(chemin[i] == '/')
    {
      position = i;
    }
  }
  char *chem_dema = (char*) calloc(strlen(chemin), sizeof(char));
  strncpy(chem_dema, chemin, position);
  position++;
  int j = 0;
  if(strcmp(chemin, "/") != 0)
  {
    while(1)
    {
      if(strcmp(tab_disque[j]->repertoire->liste->path_dir, chemin) == 0)
      {
        if(strcmp(tab_disque[j]->repertoire->liste->nom, chemin + position) == 0)
        {
          tab_disque[j]->repertoire->liste->inode->taille += taille_pere;
          return 1;
        }
      }
      j++;
    }
  }
  return -1;
}