/**
 * INF3172 – Principes des systèmes d'exploitation
 *
 * Euloge Nihezagire NIHE16098601 
 * TP2 – Hiver 2016 
 * Shell pour la gestion des fichiers et répertoires en Unix.
 *
 */
#ifndef STRUCTURE_H
#define STRUCTURE_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include <time.h>
#include <stdio.h>
  
  #define TAILLE_BUFFER 150 
#define NB_BLOC_FINAL 52224
#define NB_INODE_FINAL 69137
#define NB_POINTEUR 512
#define NB_TEMPS 15

typedef struct tm TEMPS;


typedef struct indirection_simple{
   int no_bloc[256];
} INDIRECTIONSIMPLE;

typedef struct indirection_double{
   INDIRECTIONSIMPLE* is[256];
   
} INDIRECTIONDOUBLE;

typedef struct indirection_triple{
   INDIRECTIONDOUBLE* id[256];
} INDIRECTIONTRIPLE;

 



typedef struct i_node{

  char mode[11];
  unsigned int nbLiens;
  int  UID;
  int  GID;
  unsigned int taille;
  TEMPS creation;
  TEMPS acces;
  TEMPS modification;
  
  int premiersBlocs[10];
  INDIRECTIONSIMPLE* isimple;
  INDIRECTIONDOUBLE* idouble;
  INDIRECTIONTRIPLE* itriple;

} INODE;





typedef struct element_ligne_bloc{

  unsigned int position;
  unsigned int bloc;
  char *nom ;
  char * path_dir;
  char * pere_dir;
  char type;
  INODE* inode;
  
} LIGNEBLOC;

typedef struct element_liste_bloc{

  LIGNEBLOC liste[6];
  struct element_liste_bloc* suivant;
  
} LISTEREPERTOIRE;

typedef struct element_repertoire{

  LISTEREPERTOIRE* repertoire;
  
} BLOCREPERTOIRE;

typedef struct element_fichier{

  unsigned int utilise;
  
} BLOC;

BLOCREPERTOIRE * creer_repertoire_fichier(BLOCREPERTOIRE **tab_disque, unsigned int position_dir, char **arg_list, char type,  char *chemin, int bloc_utiliser[NB_BLOC_FINAL]);


#ifdef	__cplusplus
}
#endif

#endif /* STRUCTURE_H */

