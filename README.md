# INF3172 Tp2
Présentation du logiciel
------------------------

> C'est un logiciel qui crée un Shell pour la gestion des fichiers et répertoires en Unix. 
> utilisant une liste chainée comme structure d'implementation.
> Le dossier contient: un fichier main.c; un fichier commande.c; un fichier structure.h; 
> un fichier Makefile et un fichier Readme.

Installation du logiciel
------------------------

> Le logiciel doit être téléchargé et la compilation se fait sur ligne de 
> commande avec le compilateur GNU GCC. Pour le compiler vous devez lancer le 
> logiciel make dans le repertoire contenant les sources qui executera le fichier  
> Makefile pour faire le build. Un fichier executable "tp2" sera créé. 


Utilisation du logiciel
-----------------------

> Le logiciel doit être lancé à la console en executant "tp2" 

> Exemple d'exécution du logiciel : ./tp2