/**
 * INF3172 – Principes des systèmes d'exploitation
 *
 * Euloge Nihezagire NIHE16098601 
 * TP2 – Hiver 2016 
 * Shell pour la gestion des fichiers et répertoires en Unix.
 */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <glob.h>
#include <glob.h>
#include "structure.h"
#include "commande.h"
#include <assert.h>


char * init_dir_root(BLOCREPERTOIRE **tab_disque);
char *return_chemin_pere(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], char *chemin, int bloc_utiliser[NB_BLOC_FINAL]);
int *rmdir_commande(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], BLOCREPERTOIRE **tab_inode, char *chemin_demander, char *chemin, int *espace_bloc);
int *rm_commande(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], BLOCREPERTOIRE **tab_inode, char *chemin_demander, char *chemin, int* espace_bloc);
int libere_bloc(int *espace_bloc, int bloc_utiliser[NB_BLOC_FINAL]);
int *retourne_bloc_libre(int *espace_bloc, int bloc_utiliser[NB_BLOC_FINAL]);

int main()
{

  BLOCREPERTOIRE * disque[][NB_POINTEUR] = {
    {0}
  };
  BLOCREPERTOIRE * tab_inode[NB_BLOC_FINAL] = {0};
  int bloc_utiliser[NB_BLOC_FINAL] = {0};
  char *chemin = (char*) calloc(2, sizeof(char));

  int processus_p = fork();
  if(processus_p == 0)
  {
    char buffer_cmd[TAILLE_BUFFER];
    int indices_disque[3] = {0, 0, 0};
    char *arg_list[32];
    chemin = init_dir_root(tab_inode);

    while(1)
    {

      printf("NewShell %s#: ", chemin);
      if(strcmp(fgets(buffer_cmd, TAILLE_BUFFER, stdin), "\n") == 0)
      {
        continue;
      }
      buffer_cmd[strlen(buffer_cmd) - 1] = '\0';
      if(strcmp("logout", buffer_cmd) == 0)
      {
        exit(0);
      }

      char *cmd = strdup(buffer_cmd);
      char *tmp = strtok(cmd, " ");
      int increment = 0;
      while(tmp != NULL)
      {

        arg_list[increment] = strdup(tmp);
        increment++;
        tmp = strtok(NULL, " ");

      }

      arg_list[increment] = NULL;


      //  ********** Traitement des commandes ******************

      //  ********** commande ls ******************
      if(strcmp(arg_list[0], "ls") == 0)
      {
        if(arg_list[1] != NULL && strcmp(arg_list[1], "-l") == 0)
        {
          arg_list[1] = arg_list[2];
          arg_list[2] = "-l";
        }

        // traiment dans le dossier courant sans chemin 
        if(arg_list[1] == NULL)
        {
          ls_commande(disque, chemin, bloc_utiliser, tab_inode, arg_list);
        }
          // traiment dans le dossier courant avec chemin vers un autre dossier
        else
          if(arg_list[1] != NULL && strcmp(chemin, "/") != 0 && *arg_list[1] != '/')
        {
          char *chemin_demander = (char*) calloc(strlen(chemin) + strlen(arg_list[1]) + 1, sizeof(char));
          strcat(chemin_demander, chemin);
          strcat(chemin_demander, "/");
          strcat(chemin_demander, arg_list[1]);

          ls_commande(disque, chemin_demander, bloc_utiliser, tab_inode, arg_list);
        }
          // traiment depuis la racine avec chemin vers un autre dossier
        else
          if(arg_list[1] != NULL && strcmp(chemin, "/") == 0 && *arg_list[1] != '/')
        {
          char *chemin_demander = (char*) calloc(strlen(chemin) + strlen(arg_list[1]) + 1, sizeof(char));
          strcat(chemin_demander, "/");
          strcat(chemin_demander, arg_list[1]);

          ls_commande(disque, chemin_demander, bloc_utiliser, tab_inode, arg_list);
        }
        else
          if(arg_list[1] != NULL && *arg_list[1] == '/')
        {
          ls_commande(disque, arg_list[1], bloc_utiliser, tab_inode, arg_list);
        }

      }
        //  ********** commande cd ******************
        //  utilise le meme principes traitement des chemins que ls 
      else
        if(strcmp(arg_list[0], "cd") == 0)
      {
        char *chemin_demander = (char*) calloc(strlen(chemin) + strlen(arg_list[1]) + 1, sizeof(char));
        if(*arg_list[1] == '/' && strlen(arg_list[1]) > 1)
        {
          strcat(chemin_demander, arg_list[1]);
          chemin = cd_commande(disque, chemin_demander, chemin, bloc_utiliser, arg_list);
        }
        else
          if(strcmp(arg_list[1], "/") == 0)
        {
          chemin = "/";
        }
        else
          if(strcmp(arg_list[1], "..") != 0)
        {
          strcat(chemin_demander, chemin);
          if(strcmp(chemin, "/") != 0)
          {
            strcat(chemin_demander, "/");
          }
          strcat(chemin_demander, arg_list[1]);

          chemin = cd_commande(disque, chemin_demander, chemin, bloc_utiliser, arg_list);
        }
        else
          if(strcmp(arg_list[1], "..") == 0 && strlen(return_chemin_pere(disque, chemin, bloc_utiliser)) != 0)
        {
          chemin = return_chemin_pere(disque, chemin, bloc_utiliser);
        }
        else
          if(strcmp(arg_list[1], "..") == 0 && strlen(return_chemin_pere(disque, chemin, bloc_utiliser)) == 0)
        {
          chemin = "/";
        }
      }

        //  ********** commande mkdir ******************
        //  utilise le meme principes traitement des chemins que ls
      else
        if(strcmp(arg_list[0], "mkdir") == 0)
      {

        if(*arg_list[1] != '/' && strstr(arg_list[1], "/") == NULL)
        {
          int index = mkdir_commande(tab_inode, arg_list, chemin, bloc_utiliser);

          // Met dans indices_disque les prochains indice libre et met a jour bloc_utiliser
          retourne_bloc_libre(indices_disque, bloc_utiliser);
          tab_inode[index]->repertoire->liste->bloc = indices_disque[1];
          disque[indices_disque[1]][indices_disque[2]] = tab_inode[index];
        }

          // permet d aller cherche le nom dans le chemin 
        else
          if(*arg_list[1] != '/' && strstr(arg_list[1], "/") != NULL)
        {
          int position = -1;
          int i;
          for(i = 0;i < strlen(arg_list[1]);i++)
          {
            if(arg_list[1][i] == '/')
            {
              position = i;
            }
          }

          char *chemin_demander = (char*) calloc(strlen(chemin) + strlen(arg_list[1]) + 1, sizeof(char));
          char *nom_fich = (char*) calloc(strlen(arg_list[1]), sizeof(char));
          if(strcmp(chemin, "/") != 0)
          {
            strcat(chemin_demander, chemin);
          }
          strcat(chemin_demander, "/");
          strncpy(nom_fich, arg_list[1], position);
          strcat(chemin_demander, nom_fich);
          arg_list[1] = &arg_list[1][position + 1];

          int index = mkdir_commande(tab_inode, arg_list, chemin_demander, bloc_utiliser);
          retourne_bloc_libre(indices_disque, bloc_utiliser);
          tab_inode[index]->repertoire->liste->bloc = indices_disque[1];
          disque[indices_disque[1]][indices_disque[2]] = tab_inode[index];

        }
        else
          if(*arg_list[1] == '/')
        {
          int position = -1;
          int i;
          for(i = 0;i < strlen(arg_list[1]);i++)
          {
            if(arg_list[1][i] == '/')
            {
              position = i;
            }
          }
          char *chemin_demander = (char*) calloc(strlen(chemin) + strlen(arg_list[1]) + 1, sizeof(char));
          if(position != 0)
          {
            strncpy(chemin_demander, arg_list[1], position);
          }
          else
          {
            strcat(chemin_demander, "/");
          }
          arg_list[1] = &arg_list[1][position + 1];

          int index = mkdir_commande(tab_inode, arg_list, chemin_demander, bloc_utiliser);
          retourne_bloc_libre(indices_disque, bloc_utiliser);
          tab_inode[index]->repertoire->liste->bloc = indices_disque[1];
          disque[indices_disque[1]][indices_disque[2]] = tab_inode[index];
        }
      }

        //  ********** commande rmdir ******************
        //  utilise le meme principe de traitement des chemins que ls
      else
        if(strcmp(arg_list[0], "rmdir") == 0)
      {

        char *chemin_demander = (char*) calloc(strlen(arg_list[1]) + 1, sizeof(char));

        if(*arg_list[1] != '/' && strstr(arg_list[1], "/") == NULL)
        {
          strcat(chemin_demander, arg_list[1]);
          int *retour_position = rmdir_commande(disque, tab_inode, chemin_demander, chemin, indices_disque);
          libere_bloc(retour_position, bloc_utiliser);

        }
        else
          if(*arg_list[1] != '/' && strstr(arg_list[1], "/") != NULL)
        {
          int position = -1;
          int i;
          for(i = 0;i < strlen(arg_list[1]);i++)
          {
            if(arg_list[1][i] == '/')
            {
              position = i;
            }
          }

          char *chemin_demander = (char*) calloc(strlen(chemin) + strlen(arg_list[1]) + 1, sizeof(char));
          char *nom_dossier = (char*) calloc(strlen(arg_list[1]), sizeof(char));
          if(strcmp(chemin, "/") != 0)
          {
            strcat(chemin_demander, chemin);
          }
          strcat(chemin_demander, "/");
          strncpy(nom_dossier, arg_list[1], position);
          arg_list[1] = &arg_list[1][position + 1];
          strcat(chemin_demander, nom_dossier);

          int *retour_position = rmdir_commande(disque, tab_inode, arg_list[1], chemin_demander, indices_disque);
          libere_bloc(retour_position, bloc_utiliser);
        }
        else
          if(*arg_list[1] == '/')
        {
          int pos = -1;
          int i;
          for(i = 0;i < strlen(arg_list[1]);i++)
          {
            if(arg_list[1][i] == '/')
            {
              pos = i;
            }
          }
          char *chem_dema = (char*) calloc(strlen(arg_list[1]), sizeof(char));
          strncpy(chem_dema, arg_list[1], pos);
          if(pos != 0)
          {
            arg_list[1] = &arg_list[1][pos + 1];
            strcat(chemin_demander, arg_list[1]);
          }
          else
          {
            strcat(chemin_demander, "/");
          }

          int *retour_position = rmdir_commande(disque, tab_inode, chemin_demander, chem_dema, indices_disque);
          libere_bloc(retour_position, bloc_utiliser);
        }

      }

        //  ********** commande crf ******************
        // identique a mkdir mais sera un fichier au lieur d dossier
      else
        if(strcmp(arg_list[0], "crf") == 0)
      {
        int index = crf_commande(tab_inode, arg_list, chemin, bloc_utiliser);
        retourne_bloc_libre(indices_disque, bloc_utiliser);
        tab_inode[index]->repertoire->liste->bloc = indices_disque[1];
        disque[indices_disque[1]][indices_disque[2]] = tab_inode[index];

      }

        //  ********** commande cp ******************
        // ne fait que verifier si le fichier existe et copie le chemin ses donnees dans un autre
      else
        if(strcmp(arg_list[0], "cp") == 0)
      {
        int index = contains_fichier(disque, tab_inode, arg_list[1], chemin, bloc_utiliser, arg_list);
        if(index >= 0)
        {
          indices_disque[0] = 0;
          retourne_bloc_libre(indices_disque, bloc_utiliser);

          disque[indices_disque[1]][indices_disque[2]] = tab_inode[index];
        }
      }

        //  ********** commande mv ******************
        // la fonction contains_fichier pourra changer le chemin du pere et le nom du fichier
      else
        if(strcmp(arg_list[0], "mv") == 0)
      {
        contains_fichier(disque, tab_inode, arg_list[1], chemin, bloc_utiliser, arg_list);
      }

        //  ********** commande rm ******************
      else
        if(strcmp(arg_list[0], "rm") == 0)
      {
        char *chemin_demander = (char*) calloc(strlen(chemin) + strlen(arg_list[1]) + 1, sizeof(char));
        strcat(chemin_demander, "/");
        strcat(chemin_demander, arg_list[1]);

        int *retour_position = rm_commande(disque, tab_inode, arg_list[1], chemin_demander, indices_disque);
        libere_bloc(retour_position, bloc_utiliser);
      }
        //  ********** commande mv ******************
      else
        if(strcmp(arg_list[0], "blc") == 0)
      {
        arg_list[2] = arg_list[1];
        contains_fichier(disque, tab_inode, arg_list[1], chemin, bloc_utiliser, arg_list);
      }
      else
      {
        printf("*** Erreur commande invalide \n");
      }
    }
  }
  else
  {

    wait(&processus_p);
  }
  return 1;
}
