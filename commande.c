/**
 * INF3172 – Principes des systèmes d'exploitation
 *
 * Euloge Nihezagire NIHE16098601 
 * TP2 – Hiver 2016 
 * Shell pour la gestion des fichiers et répertoires en Unix.
 *
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "commande.h"
#include "structure.h"
#include <assert.h>



int position(BLOCREPERTOIRE **tab_disque);
char * init_dir_root(BLOCREPERTOIRE **tab_disque);
int match(char text[], char pattern[]);
int *rmdir_commande(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], BLOCREPERTOIRE **tab_inode, char *chemin_demander, char *chemin, int* espace_bloc);
int *rm_commande(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], BLOCREPERTOIRE **tab_inode, char *chemin_demander, char *chemin, int* espace_bloc);
int affiche_indirection(INODE inode_fich);

void ls_commande(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], char *chemin, int bloc_utiliser[NB_BLOC_FINAL], BLOCREPERTOIRE **tab_inode, char **arg_list)
{

  int i = 0;
  int j = return_bloc_utiliser(tab_inode, chemin, bloc_utiliser);
  int cpt = 0;
  int block;
  char buffer_temps [NB_TEMPS];
  while(i < NB_POINTEUR && (bloc_utiliser[j * NB_POINTEUR] == 1 || bloc_utiliser[0] == 0))
  {
    char *chemin_parent;
    block = j * NB_POINTEUR + i;
    if(bloc_utiliser[block] == 1)
    {
      chemin_parent = tab_disque[j][i]->repertoire->liste->pere_dir;
      if(strcmp(chemin_parent, chemin) == 0)
      {
        if(strcmp(chemin, "/") != 0 && cpt == 0)
        {
          printf(". \n");
          printf(".. \n");
          cpt++;
        }
        if(arg_list[2] != NULL && strcmp(arg_list[2], "-l") == 0)
        {
          strftime(buffer_temps, NB_TEMPS, "%d %b %R", &(tab_disque[j][i]->repertoire->liste->inode->creation));
          printf("%s ", tab_disque[j][i]->repertoire->liste->inode->mode);
          printf("%2d ", tab_disque[j][i]->repertoire->liste->inode->nbLiens);
          printf("%3d ", tab_disque[j][i]->repertoire->liste->inode->UID);
          printf("%2d ", tab_disque[j][i]->repertoire->liste->inode->GID);
          printf("%5u ", tab_disque[j][i]->repertoire->liste->inode->taille);
          printf("%3s ", buffer_temps);
          printf("%s \n", tab_disque[j][i]->repertoire->liste->nom);
        }
        else
        {
          printf("%s \n", tab_disque[j][i]->repertoire->liste->nom);
        }
      }
    }
    i++;
  }

}

char *cd_commande(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], char *chemin_demander, char *chemin, int bloc_utiliser[NB_BLOC_FINAL], char **arg_list)
{
  int i = 0;
  int j = 0;
  int block;
  while(tab_disque[j][i] != 0)
  {
    while(tab_disque[j][i] != 0)
    {
      char *chemin_parent;
      chemin_parent = tab_disque[j][i]->repertoire->liste->path_dir;
      block = j * NB_POINTEUR + i;
      if(strcmp(chemin_parent, chemin_demander) == 0 && tab_disque[j][i]->repertoire->liste->type == 'd' && tab_disque[j][i]->repertoire->liste->position != 0 && bloc_utiliser[block] == 1)
      {
        return tab_disque[j][i]->repertoire->liste->path_dir;
      }
      i++;
    }
    break;
    j++;
  }

  return chemin;
}

int mkdir_commande(BLOCREPERTOIRE **tab_disque, char **arg_list, char *chemin, int bloc_utiliser[NB_BLOC_FINAL])
{

  int index = position(tab_disque);
  char type = 'd';
  tab_disque[index] = creer_repertoire_fichier(tab_disque, index, arg_list, type, chemin, bloc_utiliser);

  return index;
}

int *rmdir_commande(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], BLOCREPERTOIRE **tab_inode, char *chemin_demander, char *chemin, int* espace_bloc)
{
  int i = 0;
  int j = 0;
  int retour;

  while(tab_disque[j][i] != 0)
  {
    while(tab_disque[j][i] != 0)
    {
      char *chemin_parent;
      chemin_parent = tab_disque[j][i]->repertoire->liste->path_dir;
      retour = match(chemin_parent, chemin_demander);

      if(retour != -1 && strcmp(tab_disque[j][i]->repertoire->liste->pere_dir, chemin) == 0 && tab_disque[j][i]->repertoire->liste->type == 'd' && strcmp(tab_disque[j][i]->repertoire->liste->nom, chemin_demander) == 0)
      {

        retour = tab_disque[j][i]->repertoire->liste->position;
        tab_inode[retour] = 0;
        tab_disque[j][i]->repertoire->liste->position = 0;
        espace_bloc[1] = j;
        espace_bloc[2] = i;
        return espace_bloc;
      }
      i++;
    }
    break;
    i = 0;
    j++;
  }
  return NULL;
}

int crf_commande(BLOCREPERTOIRE **tab_disque, char **arg_list, char *chemin, int bloc_utiliser[NB_BLOC_FINAL])
{
  int index = position(tab_disque);

  char type = 'f';
  tab_disque[index] = creer_repertoire_fichier(tab_disque, index, arg_list, type, chemin, bloc_utiliser);

  return index;
}

int *rm_commande(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], BLOCREPERTOIRE **tab_inode, char *chemin_demander, char *chemin, int* espace_bloc)
{
  int i = 0;
  int j = 0;
  int retour;

  while(tab_disque[j][i] != 0)
  {
    while(tab_disque[j][i] != 0)
    {
      char *way;
      way = tab_disque[j][i]->repertoire->liste->path_dir;
      retour = match(way, chemin);
      if(retour != -1 && tab_disque[j][i]->repertoire->liste->type == 'f' && strcmp(tab_disque[j][i]->repertoire->liste->nom, chemin_demander) == 0)
      {

        retour = tab_disque[j][i]->repertoire->liste->position;
        tab_inode[retour] = 0;

        tab_disque[j][i]->repertoire->liste->position = 0;

        espace_bloc[1] = j;
        espace_bloc[2] = i;
        return espace_bloc;
      }
      i++;
    }
    break;
    j++;
  }

  return NULL;
}

int position(BLOCREPERTOIRE **tab_disque)
{
  int i = 0;
  while(tab_disque[i] != 0)
  {
    i++;
  }
  return(i);
}

int match(char text[], char pattern[])
{
  int c, d, e, text_length, pattern_length, position = -1;

  text_length = strlen(text);
  pattern_length = strlen(pattern);

  if(pattern_length > text_length)
  {
    return -1;
  }

  for(c = 0;c <= text_length - pattern_length;c++)
  {
    position = e = c;

    for(d = 0;d < pattern_length;d++)
    {
      if(pattern[d] == text[e])
      {
        e++;
      }
      else
      {
        break;
      }
    }
    if(d == pattern_length)
    {
      return position;
    }
  }

  return -1;
}

char * init_dir_root(BLOCREPERTOIRE **tab_disque)
{

  BLOCREPERTOIRE* bloc_dir_init = NULL;
  LISTEREPERTOIRE* list_dir_init = NULL;

  bloc_dir_init = (BLOCREPERTOIRE *) calloc(NB_POINTEUR, sizeof(int));
  list_dir_init = (LISTEREPERTOIRE *) malloc(sizeof(LISTEREPERTOIRE));


  list_dir_init->liste->nom = "racine";
  list_dir_init->liste->path_dir = (char*) malloc(sizeof(char));
  list_dir_init->liste->pere_dir = NULL;
  strcat(list_dir_init->liste->path_dir, "/");
  list_dir_init->liste->position = 0;
  list_dir_init->liste->type = 'd';

  list_dir_init->suivant = NULL;

  INODE* inode_fichier = NULL;
  TEMPS* time_fichier = NULL;
  time_fichier = (TEMPS *) malloc(sizeof(TEMPS));
  inode_fichier = (INODE *) malloc(sizeof(INODE));
  inode_fichier->GID = 0;
  inode_fichier->UID = 1;
  inode_fichier->acces = *time_fichier;
  inode_fichier->creation = *time_fichier;
  inode_fichier->nbLiens = 0;

  strcat(inode_fichier->mode, "-rw-r--r--");
  inode_fichier->modification = *time_fichier;
  inode_fichier->isimple = NULL;
  inode_fichier->idouble = NULL;
  inode_fichier->itriple = NULL;

  list_dir_init->liste->inode = inode_fichier;
  bloc_dir_init->repertoire = list_dir_init;
  tab_disque[0] = bloc_dir_init;

  return list_dir_init->liste->path_dir;

}

int contains_fichier(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], BLOCREPERTOIRE **tab_inode, char *nom_fichier, char *chemin, int bloc_utiliser[NB_BLOC_FINAL], char **arg_list)
{
  int i = 0;
  int j = 0;
  int bloc;
  int index = -1;
  char *arg_list_cp[32];

  char *chemin_demander = (char*) calloc(strlen(chemin) + strlen(arg_list[2]) + 1, sizeof(char));
  while(tab_disque[j][i] != 0)
  {
    while(tab_disque[j][i] != 0)
    {
      char *nom;
      nom = tab_disque[j][i]->repertoire->liste->nom;
      bloc = j * NB_POINTEUR + i;

      if(strcmp(nom, nom_fichier) == 0 && tab_disque[j][i]->repertoire->liste->type == 'f' && bloc_utiliser[bloc] == 1)
      {
        int taille = tab_disque[j][i]->repertoire->liste->inode->taille;

        // Traitement Depuis la racine
        if(*arg_list[2] == '/')
        {
          int position = -1;
          int t;

          for(t = 0;t < strlen(arg_list[2]);t++)
          {
            if(arg_list[2][t] == '/')
            {
              position = t;
            }
          }
          if(position != 0)
          {
            strncpy(chemin_demander, arg_list[2], position);
          }
          else
          {
            strcat(chemin_demander, "/");
          }
          if(strcmp(arg_list[0], "cp") == 0)
          {
            arg_list_cp[0] = arg_list[0];
            arg_list_cp[1] = &arg_list[2][position + 1];
            char str_taille[5];
            sprintf(str_taille, "%d", taille);
            arg_list_cp[2] = str_taille;
            index = crf_commande(tab_inode, arg_list_cp, chemin_demander, bloc_utiliser);
          }
          else
            if(strcmp(arg_list[0], "mv") == 0)
          {
            tab_disque[j][i]->repertoire->liste->pere_dir = chemin_demander;
            tab_disque[j][i]->repertoire->liste->path_dir = arg_list[2];
            tab_disque[j][i]->repertoire->liste->nom = &arg_list[2][position + 1];
            return -1;
          }
        }

          // Traitement Depuis la le dossier courant
        else
        {
          if(strcmp(arg_list[0], "blc") == 0)
          {
            affiche_indirection(*tab_disque[j][i]->repertoire->liste->inode);
            return -1;
          }

          int position = -1;
          int t;
          for(t = 0;t < strlen(arg_list[2]);t++)
          {
            if(arg_list[2][t] == '/')
            {
              position = t;
            }
          }
          if(position >= 0)
          {
            char *chem_dema = (char*) calloc(strlen(arg_list[2]), sizeof(char));
            if(strcmp(chemin, "/") != 0)
            {
              strcat(chemin_demander, chemin);
            }
            strcat(chemin_demander, "/");
            strncpy(chem_dema, arg_list[2], position);
            strcat(chemin_demander, chem_dema);
          }
          else
          {
            strcat(chemin_demander, "/");

          }
          if(strcmp(arg_list[0], "cp") == 0)
          {
            arg_list_cp[0] = arg_list[0];
            arg_list_cp[1] = &arg_list[2][position + 1];
            char str_taille[5];
            sprintf(str_taille, "%d", taille);
            arg_list_cp[2] = str_taille;
            index = crf_commande(tab_inode, arg_list_cp, chemin_demander, bloc_utiliser);
          }
          else
            if(strcmp(arg_list[0], "mv") == 0)
          {
            tab_disque[j][i]->repertoire->liste->pere_dir = chemin_demander;
            tab_disque[j][i]->repertoire->liste->path_dir = arg_list[2];
            tab_disque[j][i]->repertoire->liste->nom = &arg_list[2][position + 1];
            return -1;
          }
        }
        return index;
      }
      i++;
    }
    j++;
  }
  return index;
}

char *return_chemin_pere(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], char *chemin, int bloc_utiliser[NB_BLOC_FINAL])
{
  int i = 0;
  int j = 0;
  char *chemin_demander = NULL;
  while(tab_disque[j][i] != 0)
  {
    while(tab_disque[j][i] != 0)
    {
      char *way;

      way = tab_disque[j][i]->repertoire->liste->path_dir;
      if(strcmp(way, chemin) == 0)
      {
        int taille_nom = strlen(tab_disque[j][i]->repertoire->liste->nom);
        chemin_demander = (char*) calloc(strlen(chemin), sizeof(char));
        strncpy(chemin_demander, tab_disque[j][i]->repertoire->liste->path_dir, (int) strlen(chemin) - taille_nom - 1);
      }
      i++;
    }
    break;
    j++;
  }
  return chemin_demander;
}

int return_bloc_utiliser(BLOCREPERTOIRE **tab_disque, char *chemin, int bloc_utiliser[NB_BLOC_FINAL])
{
  int j = 0;
  while(strcmp(tab_disque[j]->repertoire->liste->path_dir, chemin) != 0)
  {

    j++;
  }
  return tab_disque[j]->repertoire->liste->bloc;
}

int libere_bloc(int *espace_bloc, int bloc_utiliser[NB_BLOC_FINAL])
{
  if(espace_bloc != NULL)
  {
    int bloc = espace_bloc[1];
    int ino = espace_bloc[2];
    int block = bloc * NB_POINTEUR + ino;

    if(bloc_utiliser[block] == 1)
    {
      bloc_utiliser[block] = 0;
      return 0;
    }
  }
  printf("\n*** Erreur fichier ou repertoire n'existe pas\n");
  return -1;
}

int *retourne_bloc_libre(int *espace_bloc, int bloc_utiliser[NB_BLOC_FINAL])
{
  int j = 0;
  int i = 0;
  int block = 0;
  if(bloc_utiliser != NULL)
  {
    while(j < NB_BLOC_FINAL)
    {
      while(i < NB_POINTEUR)
      {
        block = j * NB_POINTEUR + i;
        if(bloc_utiliser[block] != 1)
        {
          bloc_utiliser[block] = 1;
          espace_bloc[1] = j;
          espace_bloc[2] = i;
          return espace_bloc;
        }
        i++;
      }
      i = 0;
      j++;
    }
  }
  return NULL;
}

int affiche_indirection(INODE inode_fich)
{
  int j = 0;
  int i = 0;
  int k = 0;
  printf("\n(10)premiers Blocs :");
  while(inode_fich.premiersBlocs[j] != 0 && j < 10)
  {
    printf(" %d ", inode_fich.premiersBlocs[j]);

    j++;
  }

  j = 0;
  printf("\n \nIndirection simple :");
  while(inode_fich.taille > 20 && inode_fich.isimple->no_bloc[j] != 0)
  {
    printf(" %d ", inode_fich.isimple->no_bloc[j]);

    j++;
  }

  j = 0;
  i = 0;
  printf("\n \nIndirection double :");
  while(inode_fich.taille > 1044 && i <= 255 && inode_fich.idouble->is[i]->no_bloc[j] != 0)
  {
    while(j <= 255 && inode_fich.idouble->is[i]->no_bloc[j] != 0)
    {

      printf(" %d ", inode_fich.idouble->is[i]->no_bloc[j]);

      j++;
    }
    if(inode_fich.idouble->is[i]->no_bloc[j] == 0)
    {
      printf("\n \n");
      return -1;
    }
    j = 0;
    i++;

  }

  j = 0;
  i = 0;
  printf("\n \nIndirection triple :");
  while(inode_fich.taille > 524308 && inode_fich.itriple->id[k]->is[i]->no_bloc[j] != 0)
  {
    while(i <= 255 && inode_fich.itriple->id[k]->is[i]->no_bloc[j] != 0)
    {
      while(j <= 255 && inode_fich.itriple->id[k]->is[i]->no_bloc[j] != 0)
      {

        printf(" %d ", inode_fich.itriple->id[k]->is[i]->no_bloc[j]);

        j++;
      }
      if(inode_fich.itriple->id[k]->is[i]->no_bloc[j] == 0)
      {
        printf("\n \n");
        return -1;
      }
      j = 0;
      i++;
    }
    if(inode_fich.itriple->id[k]->is[i]->no_bloc[j] == 0)
    {
      printf("\n \n");
      return -1;
    }

    j = 0;
    i = 0;
    k++;


  }


  printf("\n \n");
  return -1;
}