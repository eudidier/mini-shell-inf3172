/**
 * INF3172 – Principes des systèmes d'exploitation
 *
 * Euloge Nihezagire NIHE16098601 
 * TP2 – Hiver 2016 
 * Shell pour la gestion des fichiers et répertoires en Unix.
 *
 */

#ifndef COMMANDE_H
#define	COMMANDE_H
#include "structure.h"

#ifdef	__cplusplus
extern "C"
{
#endif
#define TAILLE_BUFFER 150 
#define NB_BLOC_FINAL 52224
#define NB_INODE_FINAL 69137
#define NB_POINTEUR 512

void ls_commande(BLOCREPERTOIRE *tab_disque[][512], char *chemin, int bloc_utiliser[NB_BLOC_FINAL],  BLOCREPERTOIRE **tab_inode,  char **arg_list);
char *cd_commande(BLOCREPERTOIRE *tab_disque[][512], char *chemin_demander, char *chemin, int bloc_utiliser[NB_BLOC_FINAL] , char **arg_list);
int contains_fichier(BLOCREPERTOIRE *tab_disque[][512],BLOCREPERTOIRE **tab_inode, char *chemin_demander, char *chemin, int bloc_utiliser[NB_BLOC_FINAL], char **arg_list);
int mkdir_commande(BLOCREPERTOIRE **tab_disque, char **arg_list,  char *chemin, int bloc_utiliser[NB_BLOC_FINAL]);
int crf_commande(BLOCREPERTOIRE **tab_disque, char **arg_list, char *chemin, int bloc_utiliser[NB_BLOC_FINAL]);
int affiche_indirection(INODE inode_fich);
int position(BLOCREPERTOIRE **tab_disque);
char * init_dir_root(BLOCREPERTOIRE **tab_disque);
int match(char text[], char pattern[]);
int *rmdir_commande(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], BLOCREPERTOIRE **tab_inode, char *chemin_demander, char *chemin, int* espace_bloc);
int *rm_commande(BLOCREPERTOIRE *tab_disque[][NB_POINTEUR], BLOCREPERTOIRE **tab_inode, char *chemin_demander, char *chemin, int* espace_bloc);

char *return_chemin_pere(BLOCREPERTOIRE *tab_disque[][512], char *chemin, int bloc_utiliser[52224]);
int return_bloc_utiliser(BLOCREPERTOIRE **tab_disque, char *chemin, int bloc_utiliser[52224]);


#ifdef	__cplusplus
}
#endif

#endif	/* COMMANDE_H */

